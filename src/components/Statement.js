import React from "react"
import { Link } from "gatsby"
import './statement.css'

const Statement = () => { return (
    <div className="Hero">
      <div className="HeroGroup">
        <h1>Autoridade em Concreto</h1>
        <p>Asseguramos que o seu concreto atenda aos padrões de qualidade exigidos.</p>
        <Link to="/page-2/">Contato</Link>
      </div>
      </div>
)}

export default Statement;

