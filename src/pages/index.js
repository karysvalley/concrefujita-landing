import React from "react"
import { Link } from "gatsby"
import 'bootstrap/dist/css/bootstrap.min.css';
import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"
import Card from "../components/Card"
import Statement from "../components/Statement";

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <Statement />
    <Card />
    
  </Layout>
)

export default IndexPage;
